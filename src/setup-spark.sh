#! /usr/bin/env bash

#
# spark setup, to be called from cs744-setup.sh
#
HOSTNAME="Maulik@c220g2-011115"; export HOSTNAME

#
# running on local machine.
#
CLOUDLAB="wisc.cloudlab.us"; export CLOUDLAB
NODE0="${HOSTNAME}.${CLOUDLAB}"; export NODE0
CLI1="${HOSTNAME}vm-1.${CLOUDLAB}"; export CLI1
CLI2="${HOSTNAME}vm-2.${CLOUDLAB}"; export CLI2
CLI3="${HOSTNAME}vm-3.${CLOUDLAB}"; export CLI3
# all servers
echo "$NODE0
$CLI1
$CLI2
$CLI3" > servers
# only clients
echo "$CLI1
$CLI2
$CLI3" > clients

PSSHSVR="parallel-ssh -i -O StrictHostKeyChecking=no -h servers"; export PSSHSVR
PSSHCLI="parallel-ssh -i -O StrictHostKeyChecking=no -h clients"; export PSSHCLI

#
# running on primary node
#
$PSSHSVR wget https://d3kbcqa49mib13.cloudfront.net/spark-2.2.0-bin-hadoop2.7.tgz
echo "spark downloaded "

$PSSHSVR tar zvxf spark-2.2.0-bin-hadoop2.7.tgz
echo "spark unzipped "

CLI1IP=''`ssh $CLI1 hostname -i`''
CLI2IP=''`ssh $CLI2 hostname -i`''
CLI3IP=''`ssh $CLI3 hostname -i`''
ssh $CLI1 'echo '`echo "$CLI1IP";echo "$CLI2IP";echo "$CLI3IP";`' >> /users/Maulik/spark-2.2.0-bin-hadoop2.7/conf/slaves'
ssh $CLI2 'echo '`echo "$CLI1IP";echo "$CLI2IP";echo "$CLI3IP";`' >> /users/Maulik/spark-2.2.0-bin-hadoop2.7/conf/slaves'
ssh $CLI3 'echo '`echo "$CLI1IP";echo "$CLI2IP";echo "$CLI3IP";`' >> /users/Maulik/spark-2.2.0-bin-hadoop2.7/conf/slaves'

echo "slaves added"


ssh $CLI1 spark-2.2.0-bin-hadoop2.7/sbin/start-all.sh
echo "Spark started"


# uncomment below line for command to stop spark 
# ssh $CLI1 spark-2.2.0-bin-hadoop2.7/sbin/stop-all.sh

