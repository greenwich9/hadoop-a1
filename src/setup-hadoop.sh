#! /usr/bin/env bash

#
# hadoop setup, to be called from setup-server.sh
#

$PSSHSVR wget -c http://apache.mirrors.hoobly.com/hadoop/common/hadoop-2.7.6/hadoop-2.7.6.tar.gz
$PSSHSVR rm -rf hadoop-2.7.6/
$PSSHSVR tar zvxf hadoop-2.7.6.tar.gz

# update hadoop configuration.
$PSSHSVR 'sed -i "/<configuration>/a \
<property>\n\
    <name>fs.default.name</name>\n\
    <value>hdfs://"'`hostname -i`'":9000</value>\n\
</property>" hadoop-2.7.6/etc/hadoop/core-site.xml'

NAMEDIR=hadoop/data/namenode
DATADIR=hadoop/data/datanode

$PSSHSVR mkdir -p $NAMEDIR
$PSSHSVR mkdir -p $DATADIR

$PSSHSVR 'sed -i "/<configuration>/a \
<property>
<name>dfs.namenode.name.dir</name>
<value>"'$NAMEDIR'"</value>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>"'$DATADIR'"</value>
</property>" hadoop-2.7.6/etc/hadoop/hdfs-site.xml'
