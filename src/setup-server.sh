#! /usr/bin/env bash

HOSTNAME=`hostname -f`
HOSTNAME="`whoami`@${HOSTNAME%%.*}"; export HOSTNAME
CLOUDLAB="wisc.cloudlab.us"; export CLOUDLAB
NODE0="${HOSTNAME}.${CLOUDLAB}"; export NODE0
CLI1="${HOSTNAME}vm-1.${CLOUDLAB}"; export CLI1
CLI2="${HOSTNAME}vm-2.${CLOUDLAB}"; export CLI2
CLI3="${HOSTNAME}vm-3.${CLOUDLAB}"; export CLI3
PSSHSVR="parallel-ssh -i -O StrictHostKeyChecking=no -h servers"; export PSSHSVR
PSSHCLI="parallel-ssh -i -O StrictHostKeyChecking=no -h clients"; export PSSHCLI

# all servers
echo "$NODE0
$CLI1
$CLI2
$CLI3" > servers
# only clients
echo "$CLI1
$CLI2
$CLI3" > clients

# install packages
sudo apt-get -y update --fix-missing
sudo apt-get -y install pssh
$PSSHSVR sudo apt-get -y update --fix-missing
$PSSHSVR sudo apt-get -y install openjdk-8-jdk

# update the repository.
pushd hadoop-a1
git pull
popd

#
# hadoop!
#
hadoop-a1/src/setup-hadoop.sh

#
# spark
#
hadoop-a1/src/setup-spark.sh
