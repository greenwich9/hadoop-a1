#! /usr/bin/env bash

# take hostname as script parameter.
if [ ! -z $1 ]
then

    HOSTNAME=$1; export HOSTNAME
    HOSTNAME="${HOSTNAME%%.*}"
else
    echo "Need user and primary server (not VM server)."
    echo "Usage:"
    echo "    $0 user@servername"
    exit
fi

#
# running on local machine.
#
CLOUDLAB="wisc.cloudlab.us"; export CLOUDLAB
NODE0="${HOSTNAME}.${CLOUDLAB}"; export NODE0
CLI1="${HOSTNAME}vm-1.${CLOUDLAB}"; export CLI1
CLI2="${HOSTNAME}vm-2.${CLOUDLAB}"; export CLI2
CLI3="${HOSTNAME}vm-3.${CLOUDLAB}"; export CLI3

IDFILE=id_rsa; export IDFILE
if [ ! -f ./$IDFILE ]
then
    echo "You don't have a project-specific SSH key, creating one."
    ssh-genkey -t rsa -f ./$IDFILE
fi

scp ./$IDFILE* $NODE0:~/.ssh/
ssh-copy-id -i ./$IDFILE.pub $NODE0
ssh-copy-id -i ./$IDFILE.pub $CLI1
ssh-copy-id -i ./$IDFILE.pub $CLI2
ssh-copy-id -i ./$IDFILE.pub $CLI3

#
# running on primary node
#
ssh $NODE0 git clone https://gitlab.com/nickdaly/hadoop-a1
ssh $NODE0 hadoop-a1/src/setup-server.sh
